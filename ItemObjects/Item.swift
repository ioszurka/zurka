// File Name: Item.swift
// Project: Zurka
// File Type: Swift
// Author: John Crawley
// Created On: 16/08/2021
// Description:
// This file is used to create objects
// of items so we can set the name,
// desc and cost of the item and
// we can return the each from the
// getters


import Foundation

class Item
{
    //Private variables//
    private var itemName: String = ""
    private var itemDescription: String = ""
    private var itemCost: Int32 = 0
    //Default Initializer//
    init()
    {
        self.itemName = ""
        self.itemDescription = ""
        self.itemCost = 0
    }
    //Initializer//
    init(name: String, desc: String, cost: Int32)
    {
        self.itemName = name
        self.itemDescription = desc
        self.itemCost = cost
    }
    //Func: Getters//
    //Get item name//
    func getItemName() -> String
    {
        return itemName
    }
    //Get item description//
    func getItemDescription() -> String
    {
        return itemDescription
    }
    //Get item cost//
    func getItemCost() -> Int32
    {
        return itemCost
    }

}



