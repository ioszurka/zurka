// File Name: Weapons.swift
// Project: Zurka
// File Type: Swift
// Author: John Crawley
// Created On: 17/08/2021
// Description:
// This file is used to create weapon
// objects so we can set the name, desc
// cost and the power of the weapon
// and to also return each of them
// with getters

import Foundation

class Weapon: Item
{
    //Private variables//
    private var weaponPower: Int8 = 0

    //Initializer//
    init(name: String, desc: String, cost: Int32, power: Int8)
    {
        self.weaponPower = power
        super.init(name: name, desc: desc, cost: cost)
    }
    //Getter//
    //Get power//
    func getPower() -> Int8
    {
        return weaponPower
    }
    
    
}
