//
//  NPC.swift
//  NPC
//
//  Created by John Crawley on 28/08/2021.
//

import Foundation

class NPC
{
    
    //Private traits variables//
    private var name: String = ""
    private var description: String = ""
    //private
    
    
    
    //Default Initializer//
    init()
    {
        self.name = ""
        self.description = ""

    } 
    //Initializer//
    init(name: String, desc: String)
    {
        self.name = name
        self.description = desc

    }
    //Func: Getters//
    //Get item name//
    func getItemName() -> String
    {
        return name
    }
    //Get item description//
    func getItemDescription() -> String
    {
        return description
    }

    
    
}
