// File Name: Player.swift
// Project: Zurka
// File Type: Swift
// Author: John Crawley
// Created On: 27/08/2021
// Description:
// This file is used to set and
// get player stats from CoreData



import Foundation
import CoreData
import UIKit

class Player
{
    //private variables for CoreData//
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let playerFetchRequest: NSFetchRequest<PlayerStatsEntity> = PlayerStatsEntity.fetchRequest()
    
    
    //Player trait private variables//
    private var username: String = ""
    private var race: String = ""
    private var playerClass: String = ""
    private var hp: Int16 = 0
    private var vitality: Int16 = 0
    private var prayer: Int16 = 0
    private var luck: Int16 = 0
    private var strength: Int16 = 0
    private var magic: Int16 = 0
    private var range: Int16 = 0
    private var defence: Int16 = 0
    private var totalXP: Int64 = 0
    //Initializer//
    init()
    {
        self.username = ""
        self.race = ""
        self.playerClass = ""
        self.hp = 100
        self.vitality = 0
        self.prayer = 0
        self.luck = 0
        self.strength = 0
        self.range = 0
        self.defence = 0
    }
    /*
     temp' store the player stats
     */
    func setTemporaryPlayerStats(name: String, race: String, pClass: String, hp: Int16, vitality: Int16, prayer: Int16, luck: Int16, str: Int16, range: Int16, def: Int16, tXP: Int64)
    {
        self.username = name
        self.race = race
        self.playerClass = pClass
        self.hp = hp
        self.vitality = vitality
        self.prayer = prayer
        self.luck = luck
        self.strength = str
        self.range = range
        self.defence = def
        self.totalXP = tXP
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func setPlayerStats(username: String, race: String, pClass: String)
    {
        
        
        //Set the player class traits//
        switch pClass.lowercased()
        {
        case "warrior":
            vitality = 0
            prayer = 5
            luck = 0
            strength = 30
            range = 0
            defence = 10
        case "archer":
            vitality = 0
            prayer = 5
            luck = 0
            strength = 5
            range = 10
            defence = 15
        case "wizard":
            vitality = 0
            prayer = 0
            luck = 10
            strength = 4
            range = 8
            defence = 15
        default:
            vitality = 0
            prayer = 0
            luck = 0
            strength = 0
            range = 0
            defence = 0
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func setPlayerStatsInCoreData()
    {
       // NSManagedObjectContext context
      //  let context = AppDelegate.persistentContainer.viewContext
            //.persistentContainer.viewContext
        
        
        let player = PlayerStatsEntity(context: context)
        //player.use
        player.username = username
        player.hp = hp
    
    
    }
    
    
    
    /*
     private func fetchBooks() {
         // Create Fetch Request
         let fetchRequest: NSFetchRequest<Book> = Book.fetchRequest()

         // Perform Fetch Request
         persistentContainer.viewContext.perform {
             do {
                 // Execute Fetch Request
                 let result = try fetchRequest.execute()

                 // Update Books Label
                 self.booksLabel.text = "\(result.count) Books"

             } catch {
                 print("Unable to Execute Fetch Request, \(error)")
             }
         }
     }
     */
    
    
    //Return username from CoreData//
    func getUsername() -> String
    {
        var username: String = ""
        
        do
        {
            let tasks = try context.fetch(playerFetchRequest)
               for task in tasks
            {
                   username = task.username!
               }
        }
        catch
        {
            print(error)
        }
        
        return username
    }
    
    
    
    
    
    
}
