// File Name: Command.swift
// Project: Zurka
// File Type: Swift
// Author: John Crawley
// Created On: 19/04/2021
// Description:
// This file is used to breakup strings
// then progress them seperately to process
// what to execute in the program


//Import list//
import Foundation
import Combine
import NotificationCenter
import CoreData

class Command
{
    //Private variables//
    private var commandText: [String] = [String]()  //Store the user input in a string and divide each word and remove whitespace//
    //Notification Center//
    let localNotification = NotificationCenter.default
    
    //Parse text string into an array of sub-strings//
    func parseCommand(command: String)
    {
        commandText = command.components(separatedBy: " ")
    }
    //Return the parsed array string input//
    func getParsedCommand() -> [String]
    {
        return commandText
    }
    //Check if valid command//
    func checkIsCommand() -> Bool
    {
        return false
    }
    //Process Command//
    func processCommand()
    {
        //Process the verb input on the first index of the string//
        processVerbCommand()
    }
    /*
     Process the verb string of the commands
     this will use the first word of the command text array
     to check between different cases of what the action verb is
     then it will go to a function that will execute the command.
     (i.e: "eat" will load the processEat func' + "shark" will then give increase the player's HP)
     */
    func processVerbCommand()
    {
        
        for (index, element) in commandText.enumerated()
        {
            if index == 0
            {
                //Start - Switch Statement
                switch element.lowercased()
                {
                case "reno":
                    localNotification.post(name: .commandInput, object: nil, userInfo: ["reno": "> Reno Gusani is a massive cu-cu-cu-cunt"])
                case "buy":
                    print("got here - command")
                case "knock":
                    print("got here - command")
                case "lock":
                    print("got here - command")
                case "move":
                    print("got here - command")
                case "push":
                    print("got here - command")
                case "pull":
                    print("got here - command")
                case "read":
                    print("got here - command")
                case "search":
                    print("got here - command")
                case "show":
                    print("got here - command")
                case "speak":
                    print("got here - command")
                case "touch":
                    print("got here - command")
                case "turnoff":
                    print("got here - command")
                case "turnon":
                    print("got here - command")
                case "unlock":
                    print("got here - command")
                case "untie":
                    print("got here - command")
                    
                case "inventory", "i":
                    // Set the inventory data from Core Data//
                   // let inventoryData = Zurka.PlayerInventoryEntity()
                    //Store the data in a string array//
                    var inventory = [String]()
                    
                    inventory.append("420 coins")
                    inventory.append("shark")
                    inventory.append("health potion")
                    inventory.append("demon head")
                    inventory.append("temp")

                    //Note, this is throwing an error, due to possible nil/empty values
       
                    /*inventory[0] = "\(inventoryData.coins)"
                     inventory[1] = inventoryData.pouchItemSlotOne!
                     inventory[2] = inventoryData.pouchItemSlotTwo!
                     inventory[3] = inventoryData.pouchItemSlotThree!
                     inventory[4] = inventoryData.pouchItemSlotFour!
                     inventory[5] = inventoryData.pouchItemSlotFive!*/
                    
                    //Check if inventory is empty
                    if inventory.isEmpty
                    {
                        //Return message to say inventory is empty
                        localNotification.post(name: .commandInput, object: nil, userInfo: ["inventory": "> Your inventory is empty."])
                    }
                    //If not empty, return inventory//
                    else
                    {
                        var inventoryResults = ""
                        for i in inventory
                        {
                            inventoryResults.append("> " + i + "\n")
                        }
                        //Return inventory//
                        localNotification.post(name: .commandInput, object: nil, userInfo: ["inventory": "> Your inventory: \n> ----------------------------------------------------------\n" + inventoryResults + "> ----------------------------------------------------------\n"])
                    }
                    break
                case "walk":
                    print("got here - command")
                case "pick":
                    print("got here - command")
                case "drop":
                    print("got here - command")
                case "attack":
                    print("got here - command")
                case "go":
                    print("got here - command")
                case "get":
                    print("got here - command")
                case "take":
                    print("got here - command")
                case "equip":
                    print("got here - command")
                case "unequip":
                    print("got here - command")
                case "equipment":
                    print("got here - command")
                case "stats":
                    print("got here - command")
                case "look":
                    print("got here - command")
                case "examine":
                    print("got here - command")
                case "eat":
                    processEatCommand()
                case "drink":
                    processDrinkCommand()
                  break
                case "hit":
                    print("got here - command")
                case "kill":
                    print("got here - command")
                case "quit":
                    print("got here - command")
                case "use":
                    print("got here - command")
                case "wear":
                    print("got here - command")
                case "save":
                    processSaveCommand()
                case"direction":
                    print("got here - command")
                case "clear":
                    processClearCommand()
                default:
                    localNotification.post(name: .commandInput, object: nil, userInfo: ["errorCommandNotFound": "> Command not found"])
                    break
                }
                //End - Switch Statement
            }
            else
            {
                //localNotification.post(name: .commandInput, object: nil, userInfo: ["errorCommandDoesntExist": "> That command doesn't exist"])
         
            }
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //Process actions//
    /*
     Save everything from the console text to the current status
     of the game or progress made
     */
    private func processSaveCommand()
    {
        
    }
    
    
    
    /*
     Clear the console view text
     */
    private func processClearCommand()
    {
        localNotification.post(name: .commandInput, object: nil, userInfo: ["clear": "clear"])
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
     Process Drink Command:
     this function will process drinking such things
     as water or potions to improve aspects of their stats
     */
    private func processDrinkCommand()
    {
        
        /*
         Loop through the array and see if there is a secondary
         action word to execute for a command
         */

        /*
         BUG:
         Logical error. The reason it spits out the error calls
         and the drink call is because we loop over the array with, "drink" string
         and the actual drink such as, "water" so drink is not a drink so it'll spit out
         that drink doesn't exist then it loops again and finds, "water" then spits out you
         drank water.
         resulting in this problem!.
         */
        
        /*
         NOTE#1: I think I fixed it by dropping the first in the top for loop
         resulting in, "drink" being dropped
         */
        
        
        for (_ , element) in commandText.enumerated().dropFirst()
            {
                print("elements", element)
                if !element.isEmpty
                {
                    
                    //Start - Switch Statement
                    switch element.lowercased()
                    {
                    case "water":
                        // if testhp <= 90 {testhp += 20} else {testhp += 10}
                        print("#1", element)
                        localNotification.post(name: .commandInput, object: nil, userInfo: ["drinkWater": "> You drank water"])
                    case "potion":
                        print("#1", element)
                        // if testhp <= 90 {testhp += 20} else {testhp += 10}
                        localNotification.post(name: .commandInput, object: nil, userInfo: ["drinkPotion": "> You drank potion"])
                        
                    default:
                        print("#2", element)
                        //localNotification.post(name: .commandInput, object: nil, userInfo: foodDictionary)
                        localNotification.post(name: .commandInput, object: nil, userInfo: ["dontOwnDrink": "> You don't own that drink"])
                    }
                    //End - Switch Statement
                }
                /*
                 If the user didn't finish the eat command with a food and
                 left it blank, it'll output this message
                 */
                //else if index < 1
                else if element.isEmpty
                {
                    print("#3", element)
                    localNotification.post(name: .commandInput, object: nil, userInfo: ["errorDidntEnterDrink": "> You didn't enter drink to drink"])
                }
                /*
                 If the user entered a type of food to try eat
                 and wasn't a food on the list then it'll output this
                 */
                else
                {
                    print("#4", element)
                    localNotification.post(name: .commandInput, object: nil, userInfo: ["errorDrinkDoesntExist": "> That drink doesn't exist"])
                }
            }
        
        
        
        
        
        
        
        
        
        
        
        
        

        
     
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
     Process Eating command.
     This function will determine if the player has the food.
     If the player has the food, they can eat it.
     Since this is early in development, it'll allow eating of it anyways
     */
    private func processEatCommand()
    {
        /*
         Loop through the array and see if there is a secondary
         action word to execute for a command
         */
        for (index, element) in commandText.enumerated().dropFirst()
        {
            if index == 1
            {
                //Start - Switch Statement
                switch element.lowercased()
                {
                case "fish":
                    
                    localNotification.post(name: .commandInput, object: nil, userInfo: ["foodFish": "> You ate a fish"])
                case "shrimp":
                    
                    localNotification.post(name: .commandInput, object: nil, userInfo: ["foodShrimp": "> You ate a shrimp"])
                case "eel":
                    
                    localNotification.post(name: .commandInput, object: nil, userInfo: ["foodEel": "> You ate an eel"])
                case "shark":
                    
                    localNotification.post(name: .commandInput, object: nil, userInfo: ["foodShark": "> You ate a shark"])
                case "monkfish":
                    
                    localNotification.post(name: .commandInput, object: nil, userInfo: ["foodMonkfish": "> You ate a monkfish"])
                case "beef":
                    
                    localNotification.post(name: .commandInput, object: nil, userInfo: ["foodBeef": "> You ate beef"])
                case "chicken":
                    
                    localNotification.post(name: .commandInput, object: nil, userInfo: ["foodChicken": "> You ate a chicken"])
                case "bread":
                    
                    localNotification.post(name: .commandInput, object: nil, userInfo: ["foodBread": "> You ate some breed"])
                default:
                    //localNotification.post(name: .commandInput, object: nil, userInfo: foodDictionary)
                    localNotification.post(name: .commandInput, object: nil, userInfo: ["errorDontOwnThatFood": "> You don't own that food"])
                }
                //End - Switch Statement
            }
            /*
             If the user didn't finish the eat command with a food and
             left it blank, it'll output this message
             */
           //index < 1
            
            else if element.count == 0
            {
                localNotification.post(name: .commandInput, object: nil, userInfo: ["errorNoFoodEntered": "> You didn't enter food to eat"])
            }
            /*
             If the user entered a type of food to try eat
             and wasn't a food on the list then it'll output this
             */
            else
            {
                localNotification.post(name: .commandInput, object: nil, userInfo: ["errorNoFoodExists": "> The food you entered doesn't exist"])
            }
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
}
