//
//  UIPublisher.swift
//  UIPublisher
//
//  Created by John Crawley on 23/08/2021.
//

import UIKit
import Combine

extension UITextField
{
    var textfieldPublisher: AnyPublisher<String, Never>
    {
        NotificationCenter.default
        //1. Set a publisher for UITextfield //
            .publisher(for: UITextField.textDidChangeNotification, object: self)
        //2. Recieve notification with objects that are instances of UITextfields//
            .compactMap{$0.object as? UITextField}
        //3. Extract text and remove option values (even if the text cannot be nil//)
            .compactMap(\.text)
        //4. Expose an instance of any publisher to the downstream subscruber//
            .eraseToAnyPublisher()
    }
}


