//
//  NotificationCenter.swift
//  Zurka
//
//  Created by John Crawley on 11/08/2021.
//

import Combine
import NotificationCenter

extension Notification.Name
{
    static let commandInput = Notification.Name("commandInput")
    static let commandInputRecieved = Notification.Name("commandInputRecieved")
    static let commandInputTask = Notification.Name("commandInputCompleteTask")
    //static let commandInput = Notification.Name("commandInputRecieved")
}




