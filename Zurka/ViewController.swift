// File Name: ViewController.swift
// Project: Zurka
// File Type: Swift
// Author: John Crawley
// Created On: 26/04/2021
// Description:
// this view controller is the main view controller
// when the user opens the application, this is essentially
// the main menu view controller


import UIKit


class ViewController: UIViewController
{

    override func viewDidLoad()
    {
        //Super View Did Load//
        super.viewDidLoad()
    
    }


}

