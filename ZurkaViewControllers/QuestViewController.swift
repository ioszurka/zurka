// File Name: QuestViewController.swift
// Project: Zurka
// File Type: Swift
// Author: John Crawley
// Created On: 17/06/2021
// Description:
// This file is for handling the quest view controller
// and display what quests there are, percent done and what
// to do to complete them

import UIKit
/*
    Quest View Controller
    --------------------
    This view controller is to store and
    display the list of quests and the progress
    the player has made towards them.
    It will load data from Core Data to calculate
    and show any data that needs to be presented
 */
class QuestViewController: UIViewController
{
    
    
    //Variables//
    @IBOutlet weak var questTableView: UITableView! //U.I : Table View//
    //Quest progression//
  
 

    override func viewDidLoad()
    {
        //Super View Did Load//
        super.viewDidLoad()
        //Table delegate + data source//
        questTableView.delegate = self
        questTableView.dataSource = self
    }

    /*
     Quests function:
     This function allows to gather the progress value and
     the array of each quest number and name and return it
     as a String array to be used for the table view
     */
    func quests() -> [String]
    {
        var progress = [30.1,55.4,100,20,10,0,3.9,5,10,90.9,100,80]
        var list = [
            "Quest#1 \t - \t\t\t\t\t\t\t \(progress[0])%",
            "Quest#2 \t - \t\t\t\t\t\t\t \(progress[1])%",
            "Quest#3 \t - \t\t\t\t\t\t\t \(progress[2])%",
            "Quest#4 \t - \t\t\t\t\t\t\t \(progress[3])%",
            "Quest#5 \t - \t\t\t\t\t\t\t \(progress[4])%",
            "Quest#6 \t - \t\t\t\t\t\t\t \(progress[5])%",
            "Quest#7 \t - \t\t\t\t\t\t\t \(progress[6])%",
            "Quest#8 \t - \t\t\t\t\t\t\t \(progress[7])%",
            "Quest#9 \t - \t\t\t\t\t\t\t \(progress[8])%",
            "Quest#10 \t - \t\t\t\t\t\t\t \(progress[9])%",
            "Quest#11 \t - \t\t\t\t\t\t\t \(progress[10])%",
            "Quest#12 \t - \t\t\t\t\t\t\t \(progress[11])%"
        ]
        
        return list
    }
}
/*
    Extension to make Table View Delegate and Data Source in a neat manner
 */
extension QuestViewController: UITableViewDelegate, UITableViewDataSource
{
    //
    func tableView(_ tablwView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("You selected")
    }
    //
    func tableView(_ tableView: UITableView, numberOfRowsInSection: Int) -> Int
    {
        return quests().count
    }
    //
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = questTableView.dequeueReusableCell(withIdentifier: "QuestsTable", for: indexPath)
        cell.textLabel?.text = quests()[indexPath.row]
        cell.textLabel?.textColor = UIColor.white
        return cell
    }
    
}
