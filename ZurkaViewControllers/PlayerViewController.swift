// File Name: PlayerViewController.swift
// Project: Zurka
// File Type: Swift
// Author: John Crawley
// Created On: 18/06/2021
// Description:
// This is the view controller for displaying
// player stats and inventory

import UIKit
/*
    Player View Controller
    --------------------
    This view controller is to show player information
    such as the player's stats and the player's inventory
    so the player can open the application and see their stats
    and how much coins or what weapons they have on them
 */
class PlayerViewController: UIViewController
{
    
    @IBOutlet weak var textviewPlayerInformation: UITextView!
    
    //Variables//
    struct Player
    {
        var username: String
        var race: String
        var playerClass: String
        var hp: Int
        var strength: Int
        var defense: Int
        var vitality: Int
        var magic: Int
        var prayer:Int
        var range:Int
        var luck:Int
        
    
     
    }
    
    func getPlayerInformation() -> String
    {
        let test = Player(username: "Bub", race: "Elf", playerClass: "Mage", hp: 100, strength: 2, defense: 0, vitality: 100, magic: 100, prayer: 50, range: 24, luck: 0)
    
        
        let nl = "\n"
        let tab = "......................................................... "
        let info =
                """
                Player Stats:\(nl)
                Name: \(tab)\(test.username)\(nl)
                Race: \(tab)\(test.race)\(nl)
                Class: \(tab)\(test.playerClass)\(nl)
                HP: \(tab)\(test.hp)\(nl)
                Strength: \(tab)\(test.strength)\(nl)
                Defense: \(tab)\(test.defense)\(nl)
                Vitality: \(tab)\(test.vitality)\(nl)
                Magic: \(tab)\(test.magic)\(nl)
                Prayer: \(tab)\(test.prayer)\(nl)
                Range: \(tab)\(test.range)\(nl)
                Luck: \(tab)\(test.luck)
                """

        
     return info
    }

 

    override func viewDidLoad()
    {
        //Super View Did Load//
        super.viewDidLoad()
        
        textviewPlayerInformation.text = "\(getPlayerInformation())"
    }


}
