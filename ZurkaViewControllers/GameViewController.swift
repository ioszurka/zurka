// File Name: GameViewController.swift
// Project: Zurka
// File Type: Swift
// Author: John Crawley
// Created On: 20/06/2021
// Description:
// This is the game view controller
// where the player will go to play the game
// enter commands

//Import List//
import UIKit
import Combine
import CoreData

class GameViewController: UIViewController, UITextViewDelegate
{
    //U.I Elements//
    @IBOutlet weak var inputTextField: UITextField!     //Textfield - input//
    @IBOutlet weak var consoleTextView: UITextView!     //Textview - ouput//
    //Variables//
    let command = Command()                             //Create an instance of command class//
    let localNotification = NotificationCenter.default  //Notification Center//
    //View Did Load//
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
    
      //  let test = Player()
       // test.setTemporaryPlayerStats(name: "bob", race: "gob", pClass: "mage", hp: 100, vitality: 3, prayer: 3, luck: 120, str: 100, range: 0, def: 0, tXP: 200000000)
        //test.setPlayerStatsInCoreData()
        
        
       // print(test.getUsername())
        
        
        
        /*
         Make the keyboard always open when the view controller
         is viewed along with turn off auto correction
         */
        inputTextField.becomeFirstResponder()
        inputTextField.autocorrectionType = .no
        
        /*
         Without adding delegate, I can't track events in this
         U.I element
         */
        consoleTextView.delegate = self
        /*
         Set the welcome text on hte console
         */
        consoleTextView.text += "> Welcome to Zurka \n"
        /*
         Notification Center: Observer
         listen for the .commandInput singal to be radioed out
         to the program
         */
        localNotification.addObserver(self, selector: #selector(textViewUpdate), name: .commandInput, object: nil)
    }
    
    
    /*
     This button fuction will be called when the return/continue/done key
     is pressed. Process input and then clear textfield
     */
    @IBAction func btnReturnPressed(_ sender: Any)
    {
        /*
         Send the command input to command to parse and process
         what to execute
         */
        command.parseCommand(command: inputTextField.text!)
        command.processCommand()
        //Clear textfield//
        inputTextField.text = ""
    }

    

    
    /*
     TextViewUpdate function that executes when the observer has got the
     sigal from the N.C
     */
    @objc func textViewUpdate(_ notification: NSNotification)
    {
        if let commandData = notification.userInfo as? [String: String]
        {

            // for (_ , value) in commandData
            for (_, value) in commandData
            {
                
                

                if value.contains("clear")
                {
                    consoleTextView.text = nil
                }
                else
                {
                    consoleTextView.text +=  value + "\n"
                    print(value)
                }
                
                
                
                
            }
            
        }
        
    }
    
    
  
 

    
    
    
    
    
    
    
    
    
    

    
    
    
    
}

/*
 func entityIsEmpty()
 {
     let context = objAppDelegate.persistentContainer.viewContext
     let fetchRequest = NSFetchRequest<myEntity>(entityName: "myEntity")

     do {
         let result = try context.fetch(fetchRequest)
         if result.isEmpty {
             print("data not exist")
             return
         } else {
             print("data exist")
         }

         for data in result {
             var obj = userNSObj()
             obj.myObject = data.myAttribute
             myArray.append(obj)
         }

     } catch {
         print(error)
     }
 }
 */
