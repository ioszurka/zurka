- [Zurka: iOS Text Adventure Game](#zurka--ios-text-adventure-game)
  * [Installation](#installation)
  * [Changelog](#changelog)
  * [Project Status](#project-status)
  * [License](#license)

# Zurka: iOS Text Adventure Game
Zurka is an offline text-adventure game built in Swift 5 and made for iOS devices (iPhone/iPad).

The Story begins with you as the player enter the world of Zurka, you as the player can decide the race and class which will change how you play the game. The world of Zurka is run by a demon lord and you must stop the demon lord by entering a dungeon full of his allies and kill the heart of the demon lord.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

## Changelog


## Project Status


## License
[MIT](https://choosealicense.com/licenses/mit/)
